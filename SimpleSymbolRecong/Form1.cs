﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Resources;

namespace SimpleSymbolRecong
{




    public partial class MainForm : Form
    {
        class Number
        {
            public int[,] pixel;
            public string symbol;

            public Number(int[,] pixel)
            {
                this.pixel = pixel;

            }
        }
        public MainForm()
        {
            InitializeComponent();
           
            LoadSamples();

        }


        List<Number> _PixelsCollection;
  

        private Number Compare(int[,] image)
        {
            int EqualsPixels;

            int BestMatchCount = 0;
            int BestMatchIndex = 0;

            for(int k=0;k< _PixelsCollection.Count;k++)
            {
                EqualsPixels = 0;

                for (int i = 0; i < 10; i++)
                {
                    for (int j = 0; j < 10; j++)
                    {
                        if(_PixelsCollection[k].pixel[i,j]==image[i,j])
                        {
                            EqualsPixels++;
                        }
                    }
                }

                if(EqualsPixels==100)
                {
                    return _PixelsCollection[k];
                }

                if(EqualsPixels>BestMatchCount)
                {
                    BestMatchCount = EqualsPixels;
                    BestMatchIndex = k;
                }

                
            }


            return _PixelsCollection[BestMatchIndex];


        }

        private int[,] ConvertFromBitmap(Bitmap image)
        {
            int[,] result = new int[10, 10];

            if (image != null)
            {
                for (int k = 0; k < image.Height; k++)
                {
                    for (int m = 0; m < image.Width; m++)
                    {

                        var pix = image.GetPixel(k, m).ToArgb();
                        if (pix == -1)
                        {

                            result[k, m] = 0;
                        }
                        else
                            result[k, m] = 1;

                    }
                }

                return result;
            }
            else
                return null;
        }


        private void LoadSamples()
        {
          
            _PixelsCollection = new List<Number>();

            for (int i = 0; i < 50; i++)
            {
                _PixelsCollection.Add(new Number(new int[10, 10]));
            }


            ResXResourceReader reader = new ResXResourceReader(@".\Resource1.resx");

            int pixIndex = 0;

            foreach (DictionaryEntry item in reader)
            {
                
                Bitmap image = (Bitmap)item.Value;
                _PixelsCollection[pixIndex].pixel = ConvertFromBitmap(image);
                _PixelsCollection[pixIndex].symbol = item.Key.ToString().Substring(1, 1);
                pixIndex++;
            }




        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            
        }

        Bitmap comparingImage;

        private void button1_Click(object sender, EventArgs e)
        {
            

            if(openFileDialog1.ShowDialog()== DialogResult.OK)
            {
                try
                {
                    if(openFileDialog1.OpenFile()!=null)
                    {
                        comparingImage = new Bitmap(openFileDialog1.FileName);
                        pictureBox2.Image = comparingImage;
                    }
                }
                catch
                {
                    ;
                }
            }



        }

        private void buttonRecognize_Click(object sender, EventArgs e)
        {
            if(comparingImage!=null && (comparingImage.Size.Height<=10 && comparingImage.Width<=10 ))
            {
                var res = Compare(ConvertFromBitmap(comparingImage));
                textBox1.Text = res.symbol;
            }
        }

       
    }
}
